#include <math.h>
 
double rk4(double(*f)(double, double), double dx, double x, double y)
{
	double	k1 = dx * f(x, y),
		k2 = dx * f(x + dx / 2, y + k1 / 2),
		k3 = dx * f(x + dx / 2, y + k2 / 2),
		k4 = dx * f(x + dx, y + k3);
	return y + (k1 + 2 * k2 + 2 * k3 + k4) / 6;
}
 
double rate(double x, double y)
{
	return x * sqrt(y);
}

double process(double dx, double x, double y, int n)
{
	double dx2 = dx/((double)n);
	//cout << dx2;
	double y2[n+1];
        y2[0] = y;
	int i;

	for (i = 1; i < n+1; i++)
		y2[i] = rk4(rate, dx2, x + dx2 * (i - 1), y2[i-1]);
 
	return y2[n];
}
