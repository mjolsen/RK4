# rk4.dsp

rk4.dsp is a collection of FAUST (
[here](faust.grame.fr)) objects that implement 4th order 
[Runge-Kutta](https://en.wikipedia.org/wiki/Runge%E2%80%93Kutta_methods)
method for the numerical solution of ordinary differential equations.
The examples show how to solve the ODE given [here](https://rosettacode.org/wiki/Runge-Kutta_method).
A version only using FAUST objects is implemented which has the caveat
that the solution stepsize must correspond to the audio rate. A second
version is also implemented which uses a C++ external function to allow 
for multirate processing so that multiple iterations of the ODE can be 
performed for each audio rate sample.

**DISCLAIMER**: The simulated ODE is an exponential growth function that
starts with amplitude 1 and increases from there. It is not an audio
signal and thus you should not attempt to listen to it as damage to
your audio equipment or hearing will ensue. I recommend using
faust2plot to output the results and faust2firefox to view the signal
flow graphs of the algorithms.

---

To use this code, you need to either have FaustLive or the Faust compiler 
installed on your system (
[here](http://faust.grame.fr/download/)) or use the online 
Faust Compiler (
[here](http://faust.grame.fr/onlinecompiler/)).

---

This code is licensed with the Synthesis Tool Kit 4.3 license an MIT-style 
license (see LICENSE)

---

Comments, questions and suggestions can be directed to 
mjolsen@ccrma.stanford.edu
