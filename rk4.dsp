// rk4.dsp - an implementation of a 4th order explicit Runge-Kutta ODE solver
declare name "Faust RK4";
declare author "Michael J. Olsen (mjolsen at ccrma.stanford.edu)";
declare copyright "Michael Jorgen Olsen";
declare version "1.0";
declare license "STK-4.3"; // Synthesis Tool Kit 4.3 (MIT style license)
declare reference "https://en.wikipedia.org/wiki/Runge%E2%80%93Kutta_methods";
declare reference "https://rosettacode.org/wiki/Runge-Kutta_method";

// import stdfaust which gives access to all standard libraries
import("stdfaust.lib");

// analytic solution of the ODE
y_rosetta(t) = (1/16)*(t^2+4)^2;

// the ODE from rosetta code
f_rosetta(t,y) = t*y^(1/2);

// 4th order explicit Runge-Kutta
// t0 = starting time value
// y0 = initial state of system
//  h = stepsize per sample
//  f = function of ODE to solve (I'm assuming f takes 2 input values t and y)
rk4(t0,y0,h,f) =  (y0-y0') + yn' with 
{
    // time signal to increment time starting from
    // t0 and incrementing by h each sample
    tn = t0+(+(h)~_)-h;
    // the 4 Runge-Kutta stages
    k1(t,y) = f(t,y);
    k2_3(t,h,k,y) = f(t+0.5*h,y+0.5*h*k);
    k4(t,h,k,y) = f(t+h,y+h*k);
    // the actual process where we start with the initial
    // state and feedback the new state each sample to use in
    // the next calculation
    yn = ((+(y0-y0') <: ((k1(tn)<:(_,_)),par(i,4,_)) : 
                        (_,(k2_3(tn,h)<:(*(2),_)),par(i,3,_)) :
                        (((_,_):>_),(k2_3(tn,h)<:(*(2),_)),par(i,2,_)) :
                        (((_,_):>_),k4(tn,h),_) : 
                        (((_,_):>*(h/6.0)),_) : +)~_);
};

// initial conditions and stepsize
t0 = 0.0;
y0 = 1.0;
h  = 1.0;
n  = 10.0; // only used with foreign function solution

// time counter
t = t0+(+(h)~_)-h;

// define foreign function that can process n RK4 iterations per 1 audio rate sample
rk4_ff = ffunction(float process(float, float, float, int),"rk4.h","");

// multirate solution using c++ foreign function
rk4_mr(t0,y0,h,f,n) = (y0-y0') + yn' with
{
    tn = t0+(+(h)~_)-h;
    yn = ((h,tn,+(y0-y0'),n) : rk4_ff)~_;
};


// Method using foreign functions (for multirate processing)
// solve ODE, display time, actual solution, RK4 solution and error
// we calculate the correct times externally and feedback the output
// as the input to the next set of iterations
// outputs: time, actual solution, RK4 solution, error
process = t, y_rosetta(t),rk4_mr(t0,y0,h,rk4_ff,n), 
          y_rosetta(t)-rk4_mr(t0,y0,h,rk4_ff,n);

// Method using only faust functions
// ODE solution occurs at audio rate which may not be ideal if 
// the error at that stepsize is too large
// outputs: time, actual solution, RK4 solution, error
//process = t,y_rosetta(t),rk4(t0,y0,h,f_rosetta),y_rosetta(t)-rk4(t0,y0,h,f_rosetta);
